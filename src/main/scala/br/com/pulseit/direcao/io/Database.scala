package br.com.pulseit.direcao.io

import scalikejdbc.{AutoSession, ConnectionPool}
import scalikejdbc._

/**
  * Created by cyro on 05/02/17.
  */
class Database {
  Class.forName("org.postgresql.Driver")
  ConnectionPool.singleton("jdbc:postgresql://localhost:5432/teste_json", "postgres", "postgres")
  implicit  val session = AutoSession

  def save(json: String) =
    sql"insert into cliente values (to_json(${json}))".update().apply()




}
