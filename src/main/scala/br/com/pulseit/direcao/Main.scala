package br.com.pulseit.direcao

import java.util.UUID

import br.com.pulseit.direcao.domain.{Cliente, Endereco}
import br.com.pulseit.direcao.io.Database
import spray.json._
/**
  * Created by cyro on 04/02/17.
  */

object Main {
  def main(args: Array[String]): Unit = {
    val end = Endereco(cep = 20730350L, logradouro = "Rua Fabio Luz", estado = "RJ", cidade = "Rio de Janeiro", bairro = "Meier")

    val cliente = Cliente(uid = UUID.randomUUID().toString,
                              nome = "Cyro Thadeu",
                              cpf = "10988806762",
                              telefone = "2134778326",
                              celular = "21987873477",
                              aceitaLavagem = true,
                              aceitaTermos = true,
                              email = "cyro.thadeu@gmail.com",
                              endereco = end,
                              numero = 275,
                              complemento = "bloco 5, apt 404")

      new Database().save(cliente.toJson.toString())
  }
}
