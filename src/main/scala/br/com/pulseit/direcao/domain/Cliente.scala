package br.com.pulseit.direcao.domain

import scala.beans.BeanProperty


/**
  * Created by cyro on 04/02/17.
  */
case class Endereco(
                    cep: Long,
                    logradouro: String,
                    cidade: String,
                    estado: String,
                    bairro: String)

case class Cliente(
                    uid: String,
                    nome: String,
                    cpf: String,
                    telefone: String,
                    celular: String,
                    aceitaLavagem: Boolean,
                    aceitaTermos: Boolean,
                    email: String,
                    endereco: Endereco,
                    numero: Int,
                    complemento: String)

