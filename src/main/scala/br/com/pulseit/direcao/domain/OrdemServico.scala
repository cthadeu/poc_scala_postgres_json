package br.com.pulseit.direcao.domain

/**
  * Created by cyro on 04/02/17.
  */
case class OrdemServico(uid: String,
                        servicos: Seq[Servico],
                        cliente: Cliente)

case class Servico(uid: String,
                   name: String,
                   descricao: String,
                   valor: Float)
