name := "direcao-autocenter"

version := "1.0"

scalaVersion := "2.11.5"

mainClass := Some("br.com.pulseit.direacao.Startup")

resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

Seq(webSettings : _*)

libraryDependencies ++= Seq(
  "org.hibernate" % "hibernate-validator" % "5.0.2.Final",
  "javax.servlet" % "javax.servlet-api" % "3.0.1" % "provided",
  "org.scalaj" %% "scalaj-http" % "2.3.0",
  "postgresql" % "postgresql" % "9.1-901.jdbc4",
  "org.eclipse.jetty" % "jetty-webapp" % "9.1.0.v20131115" % "container, compile",
  "org.eclipse.jetty" % "jetty-jsp" % "9.1.0.v20131115" % "container",
  "com.github.fommil" %% "spray-json-shapeless" % "1.3.0",
  "org.scalikejdbc" %% "scalikejdbc"       % "2.5.0"
)